import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-boydrone',
  templateUrl: './boydrone.component.html',
  styleUrls: ['./boydrone.component.less']
})
export class BoydroneComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }

  backPage(): void {
    this.router.navigate(['/present']);
  }
}
