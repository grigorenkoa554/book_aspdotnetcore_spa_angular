import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-smilekiss',
  templateUrl: './smilekiss.component.html',
  styleUrls: ['./smilekiss.component.less']
})
export class SmilekissComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }

  backPage(): void {
    this.router.navigate(['/present']);
  }
}
