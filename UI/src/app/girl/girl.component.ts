import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-girl',
  templateUrl: './girl.component.html',
  styleUrls: ['./girl.component.less']
})
export class GirlComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }

  backPage(): void {
    this.router.navigate(['/present']);
  }

}
