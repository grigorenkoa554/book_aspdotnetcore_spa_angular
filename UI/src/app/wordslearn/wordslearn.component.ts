import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Guid } from 'guid-typescript';
import { NzDrawerPlacement } from 'ng-zorro-antd/drawer';
import { NzMessageService } from 'ng-zorro-antd/message';
import { Word, WordType } from '../models';
import { WordService } from '../users/services/word.service';

@Component({
  selector: 'app-wordslearn',
  templateUrl: './wordslearn.component.html',
  styleUrls: ['./wordslearn.component.less']
})
export class WordslearnComponent implements OnInit {
  words: Word[] = [];
  wordsN: Word[] = [];
  myDemoValue = 1;
  isVisible = false;
  isViewing = false;
  isField = false;
  isColors = false;
  items: number[] = [];
  countOfWords = 5;
  colors: string[] = ['#DC143C', '#04C614', '#040EC6', '#6F04C6', '#C64B04',
    '#00BFFF', '#20B2AA', '#DA70D6', '#7B68EE', '#FF8C00', '#8B4513','#708090'];
  myColor: string;
  hideButtons = false;
  visible = false;
  placement: NzDrawerPlacement = 'left';

  constructor(private wordService: WordService, private nzMessageService: NzMessageService,
    private router: Router) { }

  ngOnInit() {
    this.init();
  }

  init() {
    this.wordService.getWords().subscribe((data) => {
      this.words = data;
    });
  }

  createMessage(word: string): void {
    this.nzMessageService.create(word, `Слово добавлено!`);
  }

  addWord(wordForLearn: string, wordType: WordType) {
    this.wordService.addWord(wordForLearn, wordType).subscribe(() => {
      this.init();
    });
  }

  deleteWord(id: Guid) {
    this.wordService.deleteWord(id).subscribe(() => {
      this.init();
    });
  }

  showModal(): void {
    this.isVisible = !this.isVisible;
  }
  showModalWords(): void {
    this.isViewing = !this.isViewing;
    this.router.navigate(['/listwords']);
  }

  handleOk(): void {
    console.log('Button ok clicked!');
    this.isVisible = false;
  }

  handleCancel(): void {
    console.log('Button cancel clicked!');
    this.isVisible = false;
    this.isViewing = false;
  }

  showField(): void {
    console.log('Button start clicked!');
    this.getWordsN(this.countOfWords);
    this.isField = true;
    this.hideButtons = true;
/*    this.router.navigate(['/carusel']);*/
    
  }

  goPresents(): void {
    this.router.navigate(['/present']);
  }

  getWordsN(n: number) {
    this.wordService.getWordsN(n).subscribe((data) => {
      this.wordsN = data;
    });
  }

  createRange(number): void {
    for (var i = 1; i <= number; i++) {
      this.items.push(i);
    }
  }

  showColors(): void {
    this.isColors = !this.isColors;
  }

  selectColor(item: string): void {
    this.myColor = item;
    this.isColors = false;
  }

  cancel(): void {
    this.nzMessageService.info('You are click cancel');
  }

  startCancel(): void {
    this.isField = false;
    this.hideButtons = false;
  }

  
  open(): void {
    this.visible = true;
  }

  close(): void {
    this.visible = false;
  }

}
