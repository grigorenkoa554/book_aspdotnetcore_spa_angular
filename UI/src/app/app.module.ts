import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { UsersComponent } from './users/users.component';
import { AboutComponent } from './about/about.component';
import { AddressesComponent } from './addresses/addresses.component';
import { NgZorroAntdModule, NZ_I18N, en_US } from 'ng-zorro-antd';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { registerLocaleData } from '@angular/common';
import en from '@angular/common/locales/en';
import { SelectAddressComponent } from './addresses/select-address/select-address.component';
import { WordsComponent } from './words/words.component';
import { WordslearnComponent } from './wordslearn/wordslearn.component';
import { AuthComponent } from './auth/auth.component';
import { ListwordsComponent } from './listwords/listwords.component';
import { CaruselComponent } from './carusel/carusel.component';
import { PresentComponent } from './present/present.component';
import { CatBicycleComponent } from './cat-bicycle/cat-bicycle.component';
import { TreeComponent } from './tree/tree.component';
import { UnicornComponent } from './unicorn/unicorn.component';
import { FlowerComponent } from './flower/flower.component';
import { BoydroneComponent } from './boydrone/boydrone.component';
import { SmilekissComponent } from './smilekiss/smilekiss.component';
import { GirlrelaxComponent } from './girlrelax/girlrelax.component';
import { RunrobotComponent } from './runrobot/runrobot.component';
import { GirlComponent } from './girl/girl.component';

registerLocaleData(en);


@NgModule({
  declarations: [
    AppComponent,
    UsersComponent,
    AboutComponent,
    AddressesComponent,
    SelectAddressComponent,
    WordsComponent,
    WordslearnComponent,
    AuthComponent,
    ListwordsComponent,
    CaruselComponent,
    PresentComponent,
    CatBicycleComponent,
    TreeComponent,
    UnicornComponent,
    FlowerComponent,
    BoydroneComponent,
    SmilekissComponent,
    GirlrelaxComponent,
    RunrobotComponent,
    GirlComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    NgZorroAntdModule,
    FormsModule,
    BrowserAnimationsModule,
    ReactiveFormsModule
  ],
  providers: [{ provide: NZ_I18N, useValue: en_US }],
  bootstrap: [AppComponent]
})
export class AppModule { }
