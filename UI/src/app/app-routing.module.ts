import { NgModule } from '@angular/core';
import { ReactiveFormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { AboutComponent } from './about/about.component';
import { AddressesComponent } from './addresses/addresses.component';
import { UsersComponent } from './users/users.component';
import { WordsComponent } from './words/words.component';
import { WordslearnComponent } from './wordslearn/wordslearn.component';
import { AuthComponent } from './auth/auth.component';
import { ListwordsComponent } from './listwords/listwords.component';
import { CaruselComponent } from './carusel/carusel.component';
import { PresentComponent } from './present/present.component';
import { CatBicycleComponent } from './cat-bicycle/cat-bicycle.component';
import { TreeComponent } from './tree/tree.component';
import { UnicornComponent } from './unicorn/unicorn.component';
import { BoydroneComponent } from './boydrone/boydrone.component';
import { SmilekissComponent } from './smilekiss/smilekiss.component';
import { GirlrelaxComponent } from './girlrelax/girlrelax.component';
import { RunrobotComponent } from './runrobot/runrobot.component';
import { FlowerComponent } from './flower/flower.component';
import { GirlComponent } from './girl/girl.component';

const routes: Routes = [
  //{ path: '', component: WelcomeComponent },
  { path: 'users', component: UsersComponent },
  { path: 'about', component: AboutComponent },
  { path: 'addresses', component: AddressesComponent },
  { path: 'words', component: WordsComponent },
  { path: '', component: WordslearnComponent },
  { path: 'auth', component: AuthComponent },
  { path: 'listwords', component: ListwordsComponent },
  { path: 'carusel', component: CaruselComponent },
  { path: 'present', component: PresentComponent },
  { path: 'catbicycle', component: CatBicycleComponent },
  { path: 'tree', component: TreeComponent },
  { path: 'unicorn', component: UnicornComponent },
  { path: 'flower', component: FlowerComponent },
  { path: 'boydrone', component: BoydroneComponent },
  { path: 'smilekiss', component: SmilekissComponent },
  { path: 'girlrelax', component: GirlrelaxComponent },
  { path: 'runrobot', component: RunrobotComponent },
  { path: 'girl', component: GirlComponent },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
