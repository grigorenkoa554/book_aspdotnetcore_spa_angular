import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})
export class AppComponent {
  title = 'MyAngularUI';
  count: number = 0;
  isWorsLearn = false;
  gridStyle = {
    width: '25%',
    textAlign: 'center'
  };
  constructor() { }
  showWordsLearn(): void {
    this.isWorsLearn = true;
  }
}
