import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Guid } from 'guid-typescript';
import { NzNotificationService } from 'ng-zorro-antd/notification';
import { Address, User } from '../models';
import { AddressService } from '../users/services/address.service';

@Component({
  selector: 'app-addresses',
  templateUrl: './addresses.component.html',
  styleUrls: ['./addresses.component.less']
})
export class AddressesComponent implements OnInit {

  addresses: Address[] = [];
  validateForm: FormGroup;

  constructor(private addressService: AddressService,
    private notification: NzNotificationService, private fb: FormBuilder) { }

  ngOnInit() {
    this.init();
  }

  init() {
    this.addressService.getAddresses().subscribe((data) => {
      this.addresses = data;
    });
    this.validateForm = this.fb.group({
      userName: [null, [Validators.required]],
      password: [null, [Validators.required]],
      remember: [true]
    });
  }

  createBasicNotification(street: string): void {
    this.notification.blank(
      `Delete`,
      `You are delete address <b>${street}</b>`,
      { nzDuration: 2000 }
    );
  }

  addAddress(street: string) {
    this.addressService.addAddress(street).subscribe(() => {
      this.init();
    });
  }

  saveAddress(addressId: Guid, street: string, userId: Guid) {
    this.addressService.editAddress(addressId, street, userId).subscribe(() => {
      this.init();
    });
  }

  deleteAddress(id: Guid) {
    this.addressService.deleteAddress(id).subscribe(() => {
      this.init();
    });
  }

  submitForm(): void {
    for (const i in this.validateForm.controls) {
      this.validateForm.controls[i].markAsDirty();
      this.validateForm.controls[i].updateValueAndValidity();
    }
  }
}
