import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Guid } from 'guid-typescript';
import { Address } from '../../models';
import { AddressService } from '../../users/services/address.service';

@Component({
  selector: 'app-select-address',
  templateUrl: './select-address.component.html',
  styleUrls: ['./select-address.component.less']
})
export class SelectAddressComponent implements OnInit {
  addresses: Address[] = [];
  @Output() onSelect = new EventEmitter<number>();

  constructor(private addressService: AddressService) { }

  ngOnInit() {
    this.init();
  }

  init() {
    this.addressService.getAddresses().subscribe(x => {
      this.addresses = x;
    });
  }

  select(id: number) {
    this.onSelect.emit(id);
  }
}
