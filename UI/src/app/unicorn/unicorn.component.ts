import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-unicorn',
  templateUrl: './unicorn.component.html',
  styleUrls: ['./unicorn.component.less']
})
export class UnicornComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }

  backPage(): void {
    this.router.navigate(['/present']);
  }
}
