import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Guid } from 'guid-typescript';
import { Word, WordType } from '../models';
import { WordService } from '../users/services/word.service';

@Component({
  selector: 'app-listwords',
  templateUrl: './listwords.component.html',
  styleUrls: ['./listwords.component.less']
})
export class ListwordsComponent implements OnInit {

  constructor(private wordService: WordService, private router: Router) { }
  editCache: { [key: string]: { edit: boolean; data: Word } } = {};
  listOfData: Word[] = [];
  isVisible = false;
  isVisibleEdit = false;
  newMaterial: string = '';
  newPrice: number = 0;
  newTypeOfFurniture: string = '';
  newDescription: string = '';
  newTitle: string = '';

  items: Word[] = [];

  ngOnInit() {
    this.init();
  }

  init() {
    this.wordService.search(this.searchValue).subscribe((data) => {
      this.items = data;
      this.listOfData = data
      this.updateEditCache();
    });
  }

  backPage(): void {
    this.router.navigate(['/']);
  }

  startEdit(id: Guid): void {
    this.editCache[id.toString()].edit = true;
  }

  cancelEdit(id: Guid): void {
    const index = this.listOfData.findIndex(item => item.id === id);
    this.editCache[id.toString()] = {
      data: { ...this.listOfData[index] },
      edit: false
    };
  }

  saveEdit(id: Guid): void {
    const index = this.listOfData.findIndex(item => item.id === id);
    const item = this.editCache[id.toString()].data;
    this.wordService.editWord(item.id, item.wordForLearn).subscribe(() => {
        Object.assign(this.listOfData[index], this.editCache[id.toString()].data);
        this.editCache[id.toString()].edit = false;
      });
  }

  updateEditCache(): void {
    this.listOfData.forEach(item => {
      this.editCache[item.id.toString()] = {
        edit: false,
        data: { ...item }
      };
    });
  }

  addWord(wordForLearn: string, wordType: WordType) {
    this.wordService.addWord(wordForLearn, wordType)
      .subscribe(() => {
        this.init();
      });
    this.isVisible = false;
  }

  deleteWord(id: Guid) {
    this.wordService.deleteWord(id).subscribe(() => {
      this.init();
    });
  }

  getNextId(): number {
    let max = 0;
    for (let i = 0; i < this.listOfData.length; i++) {
      if (+this.listOfData[i].id > max) {
        max = +this.listOfData[i].id;
      }
    }
    return max + 1;
  }

  deleteRow(id: string): void {
    this.listOfData = this.listOfData.filter(d => d.id.toString() !== id);
  }


  showModal(): void {
    this.isVisible = !this.isVisible;
  }

  handleOk(): void {
    console.log('Button ok clicked!');
    this.isVisible = false;
  }

  handleCancel(): void {
    console.log('Button cancel clicked!');
    this.isVisible = false;
  }

  handleEditCancel(): void {
    console.log('Button cancel clicked!');
    this.isVisibleEdit = false;
  }

  searchValue: string = '';

  reset(): void {
    this.searchValue = '';
    this.search();
  }

  search(): void {
    this.init();
    //this.listOfDisplayData = this.listOfData.filter((item: DataItem) => item.name.indexOf(this.searchValue) !== -1);
  }

}
