import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Guid } from 'guid-typescript';
import { Observable } from 'rxjs';
import { Address, User } from '../../models';

@Injectable({
  providedIn: 'root'
})
export class AddressService {

  constructor(private http: HttpClient) { }

  getAddresses(): Observable<Address[]> {
    return this.http.get<Address[]>('/api/addresses/getalladdresses');
  }

  addAddress(street: string): Observable<Address> {
    const model = { street };
    return this.http.post<Address>('/api/addresses/AddAddress', model);
  }

  editAddress(addressId: Guid, street: string, userId: Guid): Observable<string> {
    const model = { id: addressId.toString(), street, userId: userId.toString() };
    return this.http.put<string>('/api/addresses/EditAddress', model);
  }

  deleteAddress(id: Guid): Observable<Address> {
    return this.http.delete<Address>('/api/addresses/DeleteAddress/' + id);
  }
}
