import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Guid } from 'guid-typescript';
import { Observable } from 'rxjs';
import { Word, WordType } from '../../models';

@Injectable({
  providedIn: 'root'
})
export class WordService {

  constructor(private http: HttpClient) { }

  getWords(): Observable<Word[]> {
    return this.http.get<Word[]>('/api/words/getall');
  }

  addWord(word: string, wordType: WordType): Observable<Word> {
    const model = { word, wordType};
    return this.http.post<Word>('/api/words/AddWord', model);
  }

  deleteWord(id: Guid): Observable<Word> {
    return this.http.delete<Word>('/api/words/Delete/' + id);
  }

  getWordsN(n: number): Observable<Word[]> {
    return this.http.get<Word[]>('/api/words/wordscount/GetNeedWords/' + n);
  }

  getCollocationsN(n: number): Observable<Word[]> {
    return this.http.get<Word[]>('/api/words/wordscount/GetNeedCollocations/' + n);
  }

  search(text: string): Observable<Word[]> {
    return this.http.get<Word[]>(`/api/words/Search/${text}`);
  }

  editWord(wordId: Guid, wordForLearn: string): Observable<string> {
    const model = {
      wordId: wordId.toString(), wordForLearn
    };
    return this.http.put<string>('/api/words/EditWord', model);
  }
}
