import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Guid } from 'guid-typescript';
import { Observable } from 'rxjs';
import { User } from '../../models';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }

  getUsers(): Observable<User[]> {
    return this.http.get<User[]>('/api/users/getall');
  }

  addUser(firstName: string, lastName: string, url: string): Observable<User> {
    const model = { firstName, lastName, url };
    return this.http.post<User>('/api/users/AddUser', model);
  }
  deleteUser(id: Guid): Observable<User> {
    return this.http.delete<User>('/api/users/Delete/' + id);
  }

  editUser(id: Guid, firstName: string, lastName: string, url: string): Observable<string> {
    const model = { id: id.toString(), firstName, lastName, url };
    return this.http.put<string>('/api/users/EditUser', model);
  }
}
