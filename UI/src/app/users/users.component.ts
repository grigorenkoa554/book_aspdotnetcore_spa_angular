import { Component, OnInit } from '@angular/core';
import { Guid } from 'guid-typescript';
import { User } from '../models';
import { UserService } from './services/user.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.less']
})
export class UsersComponent implements OnInit {
  users: User[] = [];
  addressId: number = 0;
  myDemoValue = 1000;
  isVisible = false;
  isVisibleEdit = false;
  //colors: string[] = ["red","blue","yellow"];

  constructor(private userService: UserService) { }

  ngOnInit() {
    this.init();
  }

  init() {
    this.userService.getUsers().subscribe((data) => {
      this.users = data;
    });
  }

  addUser(firstName: string, lastName: string, url: string, addressId: Guid) {
    //this.userService.addUser(firstName, lastName, addressId).subscribe(() => {
    this.userService.addUser(firstName, lastName, url).subscribe(() => {
      this.init();
    });
    this.isVisible = false;
  }

  deleteUser(id: Guid) {
    this.userService.deleteUser(id).subscribe(() => {
      this.init();
    });
  }

  log(): void {
    console.log('click dropdown button');
  }

  selectAddress(id: number) {
    this.addressId = id;
  }

  saveEditedUser(id: Guid, firstName: string, lastName: string, url: string) {
    this.userService.editUser(id, firstName, lastName, url).subscribe(() => {
      this.isVisibleEdit = false;
      this.init();
    });
  }

  showModal(): void {
    this.isVisible = true;
  }

  handleOk(): void {
    console.log('Button ok clicked!');
    this.isVisible = false;
  }
  handleCancel(): void {
    console.log('Button cancel clicked!');
    this.isVisible = false;
  }

  handleEditCancel(): void {
    console.log('Button cancel clicked!');
    this.isVisibleEdit = false;
  }
}
