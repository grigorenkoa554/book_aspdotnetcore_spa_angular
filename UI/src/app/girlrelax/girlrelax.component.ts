import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-girlrelax',
  templateUrl: './girlrelax.component.html',
  styleUrls: ['./girlrelax.component.less']
})
export class GirlrelaxComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }

  backPage(): void {
    this.router.navigate(['/present']);
  }
}
