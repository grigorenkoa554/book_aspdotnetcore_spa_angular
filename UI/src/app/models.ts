import { Guid } from 'guid-typescript';

export class User {
  id: Guid;
  firstName: string;
  lastName: string;
  imgAvatarUrl: string;

  address: Address;
}
export class Address {
  id: Guid;
  street: string;
}
export class Word {
  id: Guid;
  wordForLearn: string;
  wordType: WordType;
}
export enum WordType {
  none = 0,
  noun = 1,
  adjective = 2,
  verb = 3,
  collocation = 4
}
