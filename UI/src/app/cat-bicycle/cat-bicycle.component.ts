import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-cat-bicycle',
  templateUrl: './cat-bicycle.component.html',
  styleUrls: ['./cat-bicycle.component.less']
})
export class CatBicycleComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }

  backPage(): void {
    this.router.navigate(['/present']);
  }
}
