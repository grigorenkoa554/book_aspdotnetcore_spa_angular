import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-runrobot',
  templateUrl: './runrobot.component.html',
  styleUrls: ['./runrobot.component.less']
})
export class RunrobotComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }

  backPage(): void {
    this.router.navigate(['/present']);
  }

}
