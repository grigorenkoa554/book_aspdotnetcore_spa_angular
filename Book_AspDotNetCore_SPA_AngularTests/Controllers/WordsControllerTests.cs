﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using Book_AspDotNetCore_SPA_Angular.Repository;
using Book_AspDotNetCore_SPA_Angular.Models;
using Moq;
using Book_AspDotNetCore_SPA_Angular.Service;
using System.Collections.Generic;
using FluentAssertions;
using System;
using AutoFixture;

namespace Book_AspDotNetCore_SPA_Angular.Controllers.Tests
{
	[TestClass()]
	public class WordsControllerTests
	{
		private Mock<IBaseRepository<Word>> baseWordRepositoryMock;
		private Mock<IWordService> wordServiceMock;
		private Fixture fixture;

		[TestCleanup]
		public void TestCleanup()
		{

		}

		[TestInitialize]
		public void TestInitialize()
		{
			baseWordRepositoryMock = new Mock<IBaseRepository<Word>>();
			wordServiceMock = new Mock<IWordService>();
		}

		[TestMethod()]
		public void WordsControllerTest()
		{
			var wordsController = new WordsController(baseWordRepositoryMock.Object, wordServiceMock.Object);
		}

		[TestMethod()]
		public void GetAllTest()
		{
			//arrange
			var word = new Word { WordForLearn = "sdfsdf", WordType = WordType.Adjective };
			var list = new List<Word> { word };
			baseWordRepositoryMock.Setup(x => x.GetAll()).Returns(list);
			var wordsController = new WordsController(baseWordRepositoryMock.Object, wordServiceMock.Object);

			//act
			var result = wordsController.GetAll();

			//assert
			result.Should().NotBeEmpty();
			result.First().Should().Be(word);
		}

		[TestMethod()]
		public void GetTest()
		{
			//arrange
			Guid guid = Guid.NewGuid();
			var wordExample = new Word();
			var word = new Word { Id = guid, WordForLearn = "KillThisLove", WordType = WordType.Verb };
			var wordsController = new WordsController(baseWordRepositoryMock.Object, wordServiceMock.Object);
			baseWordRepositoryMock.Setup(x => x.GetById(guid)).Returns(wordExample).Verifiable();

			//act
			var result = wordsController.Get(guid);

			//assert
			result.Should().NotBeNull();
			result.Should().Be(wordExample);
		}

		[TestMethod()]
		public void GetDemoTest()
		{
			var wordsController = new WordsController(baseWordRepositoryMock.Object, wordServiceMock.Object);
			Assert.AreEqual(wordsController.GetDemo(), "some result");
		}

		[TestMethod()]
		public void GetDemo2Test()
		{
			var wordsController = new WordsController(baseWordRepositoryMock.Object, wordServiceMock.Object);
			Assert.AreEqual(wordsController.GetDemo(), "some result");
		}

		[DataTestMethod]
		[DataRow(1)]
		[DataRow(2)]
		[DataRow(3)]
		public void GetNeedWordsTest(int countOfWords)
		{
			//arrange
			//int countOfWords = 2;
			var word1 = new Word { WordForLearn = "SeaAndPeace", WordType = WordType.Noun };
			var word2 = new Word { WordForLearn = "KillThisLove", WordType = WordType.Verb };
			var list = new List<Word> { word1, word2 };
			var wordsController = new WordsController(baseWordRepositoryMock.Object, wordServiceMock.Object);
			wordServiceMock.Setup(x => x.GetNeedWords(countOfWords)).Returns(list).Verifiable();

			//act
			var result = wordsController.GetNeedWords(countOfWords);

			//assert
			result.Should().NotBeEmpty();
			wordServiceMock.Verify(x => x.GetNeedWords(It.IsAny<int>()), Times.Exactly(1));
			wordServiceMock.Verify(x => x.GetNeedWords(countOfWords), Times.Exactly(1));
			Assert.AreEqual(result, list);
		}

		[DataTestMethod]
		[DataRow(2)]
		[DataRow(3)]
		public void SyllableGeneratorTest(int countOfSyllable)
		{
			//arrange
			var list = new List<string>();
			var wordsController = new WordsController(baseWordRepositoryMock.Object, wordServiceMock.Object);
			wordServiceMock.Setup(x => x.SyllableGenerator(countOfSyllable)).Returns(list).Verifiable();

			//act
			var result = wordsController.SyllableGenerator(countOfSyllable);

			//assert
			wordServiceMock.Verify(x => x.SyllableGenerator(It.IsAny<int>()), Times.Exactly(1));
			wordServiceMock.Verify(x => x.SyllableGenerator(countOfSyllable), Times.Exactly(1));
			Assert.AreEqual(result, list);
		}

		[DataTestMethod]
		[DataRow(2, 3)]
		public void GetWordsByNumberOfWordsInSentenceTest(int numberOfWordsInSentence, int totalNumberOfWords)
		{
			//arrange
			var word1 = new Word { WordForLearn = "I like", WordType = WordType.Collocation };
			var word2 = new Word { WordForLearn = "I sweem", WordType = WordType.Collocation };
			var word3 = new Word { WordForLearn = "I draw", WordType = WordType.Collocation };
			var list = new List<Word> { word1, word2, word3 };
			var wordsController = new WordsController(baseWordRepositoryMock.Object, wordServiceMock.Object);
			wordServiceMock.Setup(x => x.GetWordsByNumberOfWordsInSentence(numberOfWordsInSentence, totalNumberOfWords))
				.Returns(list).Verifiable();

			//act
			var result = wordsController.GetWordsByNumberOfWordsInSentence(numberOfWordsInSentence, totalNumberOfWords);

			//assert
			result.Should().NotBeEmpty();
			Assert.AreEqual(result, list);
			result.Should().BeEquivalentTo(list);
		}

		[TestMethod()]
		public void AddWordTest()
		{
			// arrange
			var controller = new WordsController(baseWordRepositoryMock.Object, wordServiceMock.Object);
			var addWord = new AddWordModel { Word = "Some", WordType = WordType.Adjective };
			var addWord2 = new AddWordModel { Word = "Some2", WordType = WordType.Adjective };

			// act
			controller.AddWord(addWord);

			// assert
			wordServiceMock.Verify(x => x.AddWord(It.IsAny<AddWordModel>()), Times.Exactly(1));
			wordServiceMock.Verify(x => x.AddWord(addWord), Times.Exactly(1));
			wordServiceMock.Verify(x => x.AddWord(addWord2), Times.Exactly(0));
		}

		[TestMethod()]
		public void EditWordTest()
		{
			// arrange
			var controller = new WordsController(baseWordRepositoryMock.Object, wordServiceMock.Object);
			var guid1 = Guid.NewGuid();
			var guid2 = Guid.NewGuid();
			var updateWord1 = new UpdateWordModel { WordId = guid1, Word = "Wolf"};
			var updateWord2 = new UpdateWordModel { WordId = guid2, Word = "Bird"};

			// act
			controller.EditWord(updateWord1);

			// assert
			wordServiceMock.Verify(x => x.UpdateWord(It.IsAny<UpdateWordModel>()), Times.Exactly(1));
			wordServiceMock.Verify(x => x.UpdateWord(updateWord1), Times.Exactly(1));
			wordServiceMock.Verify(x => x.UpdateWord(updateWord2), Times.Exactly(0));
		}

		[TestMethod()]
		public void DeleteTest()
		{
			//arrange
			Guid guid1 = Guid.NewGuid();
			Guid guid2 = Guid.NewGuid();
			var wordExample = new Word();
			var word1 = new Word { Id = guid1, WordForLearn = "First", WordType = WordType.Verb };
			var word2 = new Word { Id = guid2, WordForLearn = "Second", WordType = WordType.Noun };
			var wordsController = new WordsController(baseWordRepositoryMock.Object, wordServiceMock.Object);
			baseWordRepositoryMock.Setup(x => x.Delete(guid1)).Verifiable();

			//act
			wordsController.Delete(guid1);

			//assert
			baseWordRepositoryMock.Verify(x => x.Delete(guid1), Times.Exactly(1));
			baseWordRepositoryMock.Verify(x => x.Delete(guid2), Times.Exactly(0));
		}

		[TestMethod()]
		public void GetWordsNeedTypeTest()
		{
			//arrange
			var list = new List<Word> { };
			var wordsController = new WordsController(baseWordRepositoryMock.Object, wordServiceMock.Object);
			wordServiceMock.Setup(x => x.GetWordsNeedType(WordType.Noun)).Returns(list).Verifiable();

			//act
			var result = wordsController.GetWordsNeedType(WordType.Noun);

			//assert
			wordServiceMock.Verify(x => x.GetWordsNeedType(It.IsAny<WordType>()), Times.Exactly(1));
			Assert.AreEqual(result, list);
		}

		[TestMethod()]
		public void SearchTest()
		{
			//arrange
			var word = new Word { WordForLearn = "mystr", WordType = WordType.Adjective };
			var list = new List<Word> { word };
			var someString = "str";
			var wordsController = new WordsController(baseWordRepositoryMock.Object, wordServiceMock.Object);
			wordServiceMock.Setup(x => x.Search(someString)).Returns(list).Verifiable();

			//act
			var result = wordsController.Search(someString);

			//assert
			result.Should().NotBeEmpty();
			Assert.AreEqual(result, list);
			result.Should().BeEquivalentTo(list);
		}
	}
}