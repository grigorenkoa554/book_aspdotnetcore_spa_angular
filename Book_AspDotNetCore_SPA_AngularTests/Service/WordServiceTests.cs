﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Book_AspDotNetCore_SPA_Angular.Repository;
using Book_AspDotNetCore_SPA_Angular.Models;
using System.Collections.Generic;
using FluentAssertions;

namespace Book_AspDotNetCore_SPA_Angular.Service.Tests
{
	[TestClass()]
	public class WordServiceTests
	{
		private Mock<IBaseRepository<Word>> baseWordRepositoryMock;

		[TestCleanup]
		public void TestCleanup()
		{

		}

		[TestInitialize]
		public void TestInitialize()
		{
			baseWordRepositoryMock = new Mock<IBaseRepository<Word>>();
		}

		[TestMethod()]
		public void AddWordTest()
		{
			// arrange
			var servcie = new WordService(baseWordRepositoryMock.Object);
			var list = new List<Word>();
			var addWordModel = new AddWordModel { Word = "Some Text", WordType = WordType.Adjective };
			baseWordRepositoryMock.Setup(x => x.GetAll()).Returns(list);
			Word word = null; //new Word { WordForLearn = addWordModel.Word, WordType = addWordModel.WordType };
			baseWordRepositoryMock.Setup(x => x.Add(It.IsAny<Word>())).Callback<Word>(r =>
			{
				word = r;
			});
			baseWordRepositoryMock.Setup(x => x.GetAll()).Returns(list);

			// act 
			var result = servcie.AddWord(addWordModel);

			// assert
			result.WordForLearn.Should().Be(addWordModel.Word);
			result.WordType.Should().Be(addWordModel.WordType);
			baseWordRepositoryMock.Verify(x => x.Add(It.IsAny<Word>()), Times.Exactly(1));
			baseWordRepositoryMock.Verify(x => x.Add(word), Times.Exactly(1));
		}

		[TestMethod()]
		public void IgnoreAddWord_WordSameTest()
		{
			// arrange
			var servcie = new WordService(baseWordRepositoryMock.Object);
			var addWordModel = new AddWordModel { Word = "Some Text", WordType = WordType.Adjective };
			var list = new List<Word> { new Word { WordForLearn = addWordModel.Word, WordType = addWordModel.WordType } };
			baseWordRepositoryMock.Setup(x => x.GetAll())
				.Returns(list);
			Word word = null;
			baseWordRepositoryMock.Setup(x => x.Add(It.IsAny<Word>()))
				.Callback<Word>(r =>
			{
				word = r;
			});
			baseWordRepositoryMock.Setup(x => x.GetAll()).Returns(list);

			// act 
			var result = servcie.AddWord(addWordModel);

			// assert
			result.Should().BeNull();
			baseWordRepositoryMock.Verify(x => x.Add(It.IsAny<Word>()), Times.Exactly(0));
			baseWordRepositoryMock.Verify(x => x.Add(word), Times.Exactly(0));
		}

		[TestMethod()]
		public void WordServiceTest()
		{
			Assert.Fail();
		}

		[TestMethod()]
		public void AddWordTest1()
		{
			Assert.Fail();
		}

		[TestMethod()]
		public void UpdateWordTest()
		{
			Assert.Fail();
		}

		[TestMethod()]
		public void GetWordsByNumberOfWordsInSentenceTest()
		{
			Assert.Fail();
		}

		[TestMethod()]
		public void GetNeedWordsTest()
		{
			Assert.Fail();
		}

		[TestMethod()]
		public void GetWordsNeedTypeTest()
		{
			Assert.Fail();
		}

		[TestMethod()]
		public void SearchTest()
		{
			Assert.Fail();
		}
	}
}