﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Book_AspDotNetCore_SPA_Angular.Models
{
    [Table("User")]
    public class User : BaseEntity
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string ImgAvatarUrl { get; set; }

        public Address Address { get; set; }
        public RoleType RoleType { get; set; }
    }
    public enum RoleType
    {
        None = 0,
        Guest = 1,
        Registered = 2,
        Admin = 3
    }
}
