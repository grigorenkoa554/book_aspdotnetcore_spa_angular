﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Book_AspDotNetCore_SPA_Angular.Models
{
    [Table("Address")]
    public class Address: BaseEntity
    {
        public string Street { get; set; }

        public Guid? UserId { get; set; }
        public User User { get; set; }
    }
}
