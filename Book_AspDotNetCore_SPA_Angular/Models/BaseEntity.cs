﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Book_AspDotNetCore_SPA_Angular.Models
{
    public class BaseEntity
    {
        public Guid Id { get; set; }
    }
}
