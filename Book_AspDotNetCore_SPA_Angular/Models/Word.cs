﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Book_AspDotNetCore_SPA_Angular.Models
{
    [Table("Word")]
    public class Word : BaseEntity
    {
        public string WordForLearn { get; set; }
        public WordType WordType { get; set; }
    }

    public enum WordType
    {
        None = 0,
        Noun = 1,
        Adjective = 2,
        Verb = 3,
        Collocation = 4
    }
}