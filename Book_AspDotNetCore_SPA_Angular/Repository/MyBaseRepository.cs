﻿using Book_AspDotNetCore_SPA_Angular.Models;
using Book_AspDotNetCore_SPA_Angular.MyDbContext;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;


namespace Book_AspDotNetCore_SPA_Angular.Repository
{
    public interface IBaseRepository<T> : IDisposable
    {
        void Add(T item);
        List<T> GetAll();
        T GetById(Guid id);
        void Update(T item);
        void Delete(Guid id);

    }
    public class MyBaseRepository<T> : IBaseRepository<T> 
        where T : BaseEntity, new()
    {
        protected readonly FirstDbContext dbContext = new FirstDbContext();
        protected readonly DbSet<T> table;
        public MyBaseRepository()
        {
            table = dbContext.Set<T>();
        }
        public void Add(T item)
        {
            table.Add(item);
            dbContext.SaveChanges();
        }

        public void Delete(Guid id)
        {
            //T temp = GetById(id);
            table.Remove(new T { Id = id });
            dbContext.SaveChanges();
        }

        public void Dispose()
        {
            dbContext?.Dispose();
        }

        public List<T> GetAll()
        {
            return table.ToList();
        }

        public T GetById(Guid id)
        {
            return table.First(x => x.Id == id);
        }

        public void Update(T item)
        {
            if (item.Id == default)
            {
                throw new ArgumentException($"Id not set. It is '0'. Type of entity: {typeof(T)}");
            }
            table.Update(item);
            dbContext.SaveChanges();
        }
    }
}
