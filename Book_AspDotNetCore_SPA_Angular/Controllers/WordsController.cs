﻿using Book_AspDotNetCore_SPA_Angular.Filters;
using Book_AspDotNetCore_SPA_Angular.Models;
using Book_AspDotNetCore_SPA_Angular.Repository;
using Book_AspDotNetCore_SPA_Angular.Service;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Book_AspDotNetCore_SPA_Angular.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class WordsController : Controller
	{
		private readonly IBaseRepository<Word> baseWordRepository;
		private readonly IWordService wordService;
		public WordsController(IBaseRepository<Word> baseWordRepository, IWordService wordService)
		{
			this.baseWordRepository = baseWordRepository;
			this.wordService = wordService;
		}

		[HttpGet("[action]")]
		public IEnumerable<Word> GetAll()
		{
			using (baseWordRepository)
			{
				return baseWordRepository.GetAll();
			}
		}

		[HttpGet("{id}")]
		public Word Get(Guid id)
		{
			using (baseWordRepository)
			{
				return baseWordRepository.GetById(id);
			}
		}

		[FakeNotFoundResourceFilter]
		[HttpGet("[action]")]
		public string GetDemo()
		{
			return "some result";
		}

		[TimeWorkFilter]
		[HttpGet("[action]")]
		public string GetDemo2()
		{
			return "some result";
		}

		[HttpGet("wordscount/[action]/{n}")]
		public IEnumerable<Word> GetNeedWords(int n)
		{
			return wordService.GetNeedWords(n);
		}

		[HttpGet("wordscount/[action]/{numberOfWordsInSentence}/{totalNumberOfWords}")]
		public IEnumerable<Word> GetWordsByNumberOfWordsInSentence(int numberOfWordsInSentence, int totalNumberOfWords)
		{
			using (baseWordRepository)
			{
				return wordService.GetWordsByNumberOfWordsInSentence(numberOfWordsInSentence, totalNumberOfWords);
			}
		}

		[HttpPost("[action]")]
		public Word AddWord(AddWordModel wordForLearn)
		{
			return wordService.AddWord(wordForLearn);
		}

		[HttpPut("[action]")]
		public void EditWord(UpdateWordModel word)
		{
			wordService.UpdateWord(word);
		}

		[HttpDelete("[action]/{id}")]
		public void Delete(Guid id)
		{
			using (baseWordRepository)
			{
				baseWordRepository.Delete(id);
			}
		}

		[HttpGet("getWordsNeedType/[action]/{wordType}")]
		public IEnumerable<Word> GetWordsNeedType(WordType wordType)
		{
			using (baseWordRepository)
			{
				return wordService.GetWordsNeedType(wordType);
			}
		}

		[HttpGet("[action]")]
		[HttpGet("[action]/{str}")]
		public IEnumerable<Word> Search(string str)
		{
			return wordService.Search(str);
		}

		[HttpGet("[action]/{countOfSyllable}")]
		public IEnumerable<string> SyllableGenerator(int countOfSyllable)
		{
			return wordService.SyllableGenerator(countOfSyllable);
		}
	}
}
