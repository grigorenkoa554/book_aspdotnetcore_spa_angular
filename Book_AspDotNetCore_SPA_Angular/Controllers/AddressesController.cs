﻿using Book_AspDotNetCore_SPA_Angular.Models;
using Book_AspDotNetCore_SPA_Angular.Repository;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Book_AspDotNetCore_SPA_Angular.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AddressesController : Controller
    {
        private readonly IBaseRepository<Address> baseAddressRepository;
        public AddressesController(IBaseRepository<Address> baseAddressRepository)
        {
            this.baseAddressRepository = baseAddressRepository;
        }
        private static Address address = new Address { Id = Guid.NewGuid(), Street = "Golovackogo 2" };
        private static List<Address> addresses = new List<Address>
        {
            new Address{ Id = Guid.NewGuid(), Street = "Golovackogo 2"},
            new Address{ Id = Guid.NewGuid(), Street = "Golovackogo 5"},
            new Address{ Id = Guid.NewGuid(), Street = "Golovackogo 6"},
            address
        };

        [HttpGet("[action]")]
        public IEnumerable<Address> GetAllAddresses()
        {
            using (baseAddressRepository)
            {
                return baseAddressRepository.GetAll();
            }
        }

        [HttpGet("[action]/{id}")]
        public Address GetAddress(Guid id)
        {
            using (baseAddressRepository)
            {
                return baseAddressRepository.GetById(id);
            }
        }

        [HttpPost("[action]")]
        public void AddAddress(AddAddressModel model)
        {

            var address = new Address { Street = model.Street };
            using (baseAddressRepository)
            {
                baseAddressRepository.Add(address);
            }
            //var address = new Address
            //{
            //    Id = addresses.Count + 1,
            //    Street = addressModel.Street
            //};
            //addresses.Add(address);
        }

        [HttpPost("[action]")]
        public void AddAddressAndUserModel(AddAddressAndUserModel model)
        {
            var address = new Address { Street = model.Street, User = model.User };
            using (baseAddressRepository)
            {
                baseAddressRepository.Add(address);
            }
        }

        [HttpPut("[action]")]
        public void EditAddress(Address address)
        {
            using (baseAddressRepository)
            {
                baseAddressRepository.Update(address);
            }

            //var address = GetAddress(model.AddressId);
            //address.Street = model.Street;
            //for (var i = 0; i < addresses.Count; i++)
            //{
            //    if (addresses[i].Id == address.Id)
            //    {
            //        addresses[i] = address;
            //    }
            //}
        }

        [HttpDelete("[action]/{id}")]
        public void DeleteAddress(Guid id)
        {
            using (baseAddressRepository)
            {
                baseAddressRepository.Delete(id);
            }
        }
    }

    public class AddAddressModel
    {
        public string Street { get; set; }
    }

    public class AddAddressAndUserModel
    {
        public string Street { get; set; }
        public User User { get; set; }
    }

    public class EditAddressModel
    {
        public Guid AddressId { get; set; }
        public string Street { get; set; }
    }
}
