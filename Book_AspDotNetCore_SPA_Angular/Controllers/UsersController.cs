﻿using Book_AspDotNetCore_SPA_Angular.Models;
using Book_AspDotNetCore_SPA_Angular.Repository;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace Book_AspDotNetCore_SPA_Angular.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly IBaseRepository<User> baseUserRepository;
        public UsersController(IBaseRepository<User> baseUserRepository)
        {
            this.baseUserRepository = baseUserRepository;
        }
        //private static Address address = new Address { Id = 14, Street = "Golovackogo 2" };
        private static List<User> users = new List<User>
        {
            //_user,
            new User
            {
                Id = Guid.NewGuid(),
                FirstName = "Misha1",
                LastName = "Midvedev1",
                ImgAvatarUrl = @"https://www.w3schools.com/html/pic_trulli.jpg"
             },
            new User{
                Id = Guid.NewGuid(),
                FirstName = "Misha2",
                LastName = "Midvedev2",
                ImgAvatarUrl = @"assets/1234.jpg"
            },
            new User{ Id = Guid.NewGuid(), FirstName = "Misha3", LastName = "Midvedev34"}
        };

        [HttpGet("[action]")]
        public IEnumerable<User> GetAll()
        {
            using (baseUserRepository)
            {
                return baseUserRepository.GetAll();
            }
        }

        // GET api/<UsersController>/5
        [HttpGet("{id}")]
        public User Get(Guid id)
        {
            using (baseUserRepository)
            {
                return baseUserRepository.GetById(id);
            }
        }

        [HttpPost("[action]")]
        public void AddUser(User user)
        {
            using (baseUserRepository)
            {
                baseUserRepository.Add(user);
            }
            //var user = new User
            //{
            //    Id = users.Count + 1,
            //    FirstName = model.FirstName,
            //    LastName = model.LastName,
            //    ImgAvatarUrl = model.ImgAvatarUrl
            //};
            //users.Add(user);
        }

        [HttpPut("[action]")]
        public void EditUser(User user)
        {
            using (baseUserRepository)
            {
                baseUserRepository.Update(user);
            }
            //var user = Get(model.UserId);
            //user.FirstName = model.FirstName;
            //user.LastName = model.LastName;
            //user.ImgAvatarUrl = model.ImgAvatarUrl;
            //for (var i = 0; i < users.Count; i++)
            //{
            //    if (users[i].Id == user.Id)
            //    {
            //        users[i] = user;
            //        break;
            //    }
            //}
        }

        // DELETE api/<UsersController>/5
        [HttpDelete("[action]/{id}")]
        public void Delete(Guid id)
        {
            using (baseUserRepository)
            {
                baseUserRepository.Delete(id);
            }
        }

    }

    public class AddUserModel
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string ImgAvatarUrl { get; set; }
    }

    public class EditUserModel
    {
        public Guid UserId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string ImgAvatarUrl { get; set; }
    }
}
