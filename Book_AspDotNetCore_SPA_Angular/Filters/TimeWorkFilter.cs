﻿using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Book_AspDotNetCore_SPA_Angular.Filters
{
    public class TimeWorkFilter : Attribute, IResourceFilter
    {
        private DateTime time;
        public void OnResourceExecuting(ResourceExecutingContext context)
        {
            time = DateTime.Now;
            //context.Result = new ContentResult { Content = "Ресурс не найден" };
        }

        public void OnResourceExecuted(ResourceExecutedContext context)
        {
            var some = DateTime.Now - time;
            byte[] bytes = Encoding.ASCII.GetBytes(some.ToString());
            context.HttpContext.Response.Body.WriteAsync(bytes);
        }
    }
}
