﻿using Book_AspDotNetCore_SPA_Angular.Models;
using Book_AspDotNetCore_SPA_Angular.Repository;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;

namespace Book_AspDotNetCore_SPA_Angular.Service
{
	public interface IWordService
	{
		IEnumerable<Word> GetWordsByNumberOfWordsInSentence(int numberOfWordsInSentence, int totalNumberOfWords);
		Word AddWord(AddWordModel model);
		IEnumerable<Word> GetNeedWords(int n);
		IEnumerable<Word> GetWordsNeedType(WordType wordType);
		IEnumerable<Word> Search(string str);
		object UpdateWord(UpdateWordModel model);
		IEnumerable<string> SyllableGenerator(int countOfSyllable);
	}
	public class WordService : IWordService
	{
		private readonly IBaseRepository<Word> baseRepository;
		private static object myLock = new object();
		public WordService(IBaseRepository<Word> baseRepository)
		{
			this.baseRepository = baseRepository;
		}

		public Word AddWord(AddWordModel model)
		{
			var word = new Word
			{
				WordForLearn = model.Word,
				WordType = model.WordType
			};
			var list = baseRepository.GetAll();
			var count = 0;
			foreach (var item in list)
			{
				if (string.Equals(item.WordForLearn, word.WordForLearn,
					StringComparison.OrdinalIgnoreCase))
				{
					count = -1;
				}
			}
			if (count == 0)
			{
				baseRepository.Add(word);
				return word;
			}

			return null;
		}
		public object UpdateWord(UpdateWordModel model)
		{
			var exists = baseRepository
				.GetAll()
				.SingleOrDefault(x => x.Id == model.WordId);
			if (exists == null)
			{
				throw new ArgumentException($"Word with id = {model.WordId} does not exist!");
			}
			exists.WordForLearn = model.Word;
			baseRepository.Update(exists);

			return new { res = "All updated" };
		}

		public IEnumerable<Word> GetWordsByNumberOfWordsInSentence(int numberOfWordsInSentence, int totalNumberOfWords)
		{
			var words = baseRepository.GetAll()
				.Where(x => x.WordForLearn.Split(' ').Length == numberOfWordsInSentence).ToList();
			foreach (var item in words)
			{
				var count = 0;
				if (item.WordForLearn.Split(' ').Length == numberOfWordsInSentence)
				{
					count++;
				}
				if (count == 0)
				{
					words = baseRepository.GetAll().
						Where(x => x.WordForLearn.Split(' ').Length == 1).ToList();
				}
			}
			if (totalNumberOfWords > words.Count)
			{
				totalNumberOfWords = words.Count;
			}
			if (totalNumberOfWords <= 0)
			{
				totalNumberOfWords = 1;
			}
			var selected = new List<Word>();
			var needed = totalNumberOfWords;
			foreach (var item in words)
			{
				var randomWord = words[new Random().Next(0, words.Count)];
				if (needed != 0 && !selected.Contains(randomWord))
				{
					selected.Add(randomWord);
					needed--;
				}
			}
			return selected;
		}

		public IEnumerable<Word> GetNeedWords(int n)
		{
			List<Word> newWords = new List<Word>();
			int outCount = 0;
			var words = baseRepository.GetAll().Where(x => x.WordType != WordType.Collocation).ToList();
			if (n > words.Count)
			{
				n = words.Count;
			}
			if (n <= 0)
			{
				n = 1;
			}
			Random random = new Random();
			int c = words.Count;
			while (c > 1)
			{
				c--;
				int k = random.Next(n + 1);
				var value = words[k];
				words[k] = words[c];
				words[c] = value;
			}
			foreach (var item in words)
			{
				if (n != outCount)
				{
					newWords.Add(item);
					outCount++;
				}
				else
				{
					break;
				}
			}
			return newWords;
		}

		public IEnumerable<Word> GetWordsNeedType(WordType wordType)
		{
			var words = baseRepository.GetAll().Where(x => x.WordType == wordType);
			return words;
		}

		public IEnumerable<Word> Search(string str)
		{
			if (string.IsNullOrEmpty(str))
			{
				return baseRepository.GetAll();
			}
			else
			{
				str = str.ToLower();
				var items = baseRepository.GetAll()
					.Where(x => x.WordForLearn.ToLower().Contains(str)
					|| Equals(x.WordType, str.ToString())).ToList();
				return items;
			}
		}
		public IEnumerable<string> SyllableGenerator(int countOfSyllable)
		{
			Random rnd = new Random();
			string[] consonants = { "б", "в", "г", "д", "ж", "з", "к", "л", "м", "н", "п", "р", "с", "т", "ф", "х", "ц", "ч", "ш", "щ" };
			string[] vowels = { "а", "о", "у", "ы", "э", "я", "ё", "ю", "и", "е" };
			string word = "";
			var list = new List<string>();
			while (countOfSyllable != 0)
			{
				word += GetRandomLetter(rnd, consonants) + GetRandomLetter(rnd, vowels);
				list.Add(word);
				word = "";
				countOfSyllable--;
			}
			return list;
		}

		private static string GetRandomLetter(Random rnd, string[] letters)
		{
			return letters[rnd.Next(0, letters.Length - 1)];
		}


	}

	public class AddWordModel
	{
		public string Word { get; set; }
		public WordType WordType { get; set; }
	}

	public class UpdateWordModel
	{
		public Guid WordId { get; set; }
		[Required]
		public string Word { get; set; }
		//public WordType WordType { get; set; }
	}
}
