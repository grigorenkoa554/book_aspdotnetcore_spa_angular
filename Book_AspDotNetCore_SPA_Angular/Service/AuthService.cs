﻿//using Book_AspDotNetCore_SPA_Angular.Models;
//using Book_AspDotNetCore_SPA_Angular.Repository;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Threading.Tasks;

//namespace Book_AspDotNetCore_SPA_Angular.Service
//{
//    public interface IAuthService
//    {
//        Guid CurrentUserId { get; set; }
//        bool IsCurrentUserAdmin();
//        bool IsCurrentUserGuest();
//        bool IsCurrentUserRegistered();
//    }

//    public class AuthService : IAuthService
//    {
//        private readonly IBaseRepository<User> userRepository;

//        public AuthService(IBaseRepository<User> userRepository)
//        {
//            this.userRepository = userRepository;
//        }
//        public Guid CurrentUserId { get; set; }

//        public bool IsCurrentUserAdmin()
//        {
//            var user = userRepository.GetById(CurrentUserId);
//            if (user == null)
//            {
//                throw new ArgumentException("User not exist");
//            }
//            return user.RoleType == RoleType.Admin;
//        }

//        public bool IsCurrentUserGuest()
//        {
//            var user = userRepository.GetById(CurrentUserId);
//            if (user == null)
//            {
//                return true;
//            }
//            return user.RoleType == RoleType.Guest;
//        }

//        public bool IsCurrentUserRegistered()
//        {
//            var user = userRepository.GetById(CurrentUserId);
//            if (user == null)
//            {
//                throw new ArgumentException("User not exist");
//            }
//            return user.RoleType == RoleType.Registered;
//        }
//    }
//}
