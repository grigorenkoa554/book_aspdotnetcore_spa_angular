﻿using Book_AspDotNetCore_SPA_Angular.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Book_AspDotNetCore_SPA_Angular.MyDbContext
{
    public class FirstDbContext : DbContext
    {
        public FirstDbContext()
        {
            Database.EnsureCreated(); 
        }
        public DbSet<User> Users { get; set; }
        public DbSet<Address> Addresses { get; set; }
        public DbSet<Word> Words { get; set; }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder) // конфигурирование
        {
            var cs = @"Data Source = 'DESKTOP-QP982B5'; 
                Initial Catalog='DB_for_SPA_Angular';
                Integrated Security=true;"; 
            optionsBuilder.UseSqlServer(cs); 
        }
    }
}
